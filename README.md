[![Gitpod Ready-to-Code](https://img.shields.io/badge/Gitpod-Ready--to--Code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/3stadt/garage-happy-birthday) 

# Task

> Write a program that does stuff with birthdays

## 1

The program should write "Happy Birthday" on the console

```go
package main

import "fmt"

func main() {
	fmt.Println("Happy Birthday")
}
```

## 2

The program should print a birth date on the console

```go
package main

import (
	"fmt"
	"time"
)

type hacker struct {
	FirstName string
	LastName  string
	Birthday  time.Time
}

func createBDay(value string) time.Time {
	bDay, _ := time.Parse("2006-01-02 -0700", value)
	return bDay
}

func (h hacker) bDayToString() string {
	formatted := h.Birthday.Format("02.01.2006")
	return formatted
}

func main() {

	hackerWoman := hacker{
		FirstName: "Hacker",
		LastName:  "Woman",
		Birthday:  createBDay("1982-02-05 +0200"),
	}

	hwbs := "Hackerwomans Geburtstag: "
	hwbs = hwbs + hackerWoman.bDayToString()

	fmt.Println(hwbs)
}
```

## 3

The program should print a name as well

```go
package main

import (
	"fmt"
	"time"
)

type hacker struct {
	FirstName string
	LastName  string
	Birthday  time.Time
}

func createBDay(value string) time.Time {
	bDay, _ := time.Parse("2006-01-02 -0700", value)
	return bDay
}

func (h hacker) bDayToString() string {
	formatted := h.Birthday.Format("02.01.2006")
	return formatted
}

func main() {

	sb := hacker{
		FirstName: "Susanne",
		LastName:  "Breger",
		Birthday:  createBDay("1982-02-05 +0200"),
	}

	fmt.Printf("%s %s's Birthday: %s\n", sb.FirstName, sb.LastName, sb.bDayToString())
}
```

## 4

The text from #4 should be printed 100 times

```go
package main

import (
	"fmt"
	"time"
)

type hacker struct {
	FirstName string
	LastName  string
	Birthday  time.Time
}

func createBDay(value string) time.Time {
	bDay, _ := time.Parse("2006-01-02 -0700", value)
	return bDay
}

func (h hacker) bDayToString() string {
	formatted := h.Birthday.Format("02.01.2006")
	return formatted
}

func main() {

	sb := hacker{
		FirstName: "Susanne",
		LastName:  "Breger",
		Birthday:  createBDay("1982-02-05 +0200"),
	}

	for i := 0; i < 100; i++ {
		fmt.Printf("%s %s's Birthday: %s\n", sb.FirstName, sb.LastName, sb.bDayToString())
	}
}
```

## 5

The program should write the current line number from 1 to 100

```go
package main

import (
	"fmt"
	"time"
)

type hacker struct {
	FirstName string
	LastName  string
	Birthday  time.Time
}

func createBDay(value string) time.Time {
	bDay, _ := time.Parse("2006-01-02 -0700", value)
	return bDay
}

func (h hacker) bDayToString() string {
	formatted := h.Birthday.Format("02.01.2006")
	return formatted
}

func main() {

	sb := hacker{
		FirstName: "Susanne",
		LastName:  "Breger",
		Birthday:  createBDay("1982-02-05 +0200"),
	}

	for i := 0; i < 100; i++ {
		fmt.Printf("[%03d] %s %s's Birthday: %s\n", i+1, sb.FirstName, sb.LastName, sb.bDayToString())
	}
}

```

## 6

Instead of printing a lot of the same text, the program should print birthday greetings for different persons

```go
package main

import (
	"fmt"
	"time"
)

type hacker struct {
	FirstName string
	LastName  string
	Birthday  time.Time
}

func createBDay(value string) time.Time {
	bDay, _ := time.Parse("2006-01-02 -0700", value)
	return bDay
}

func (h hacker) bDayToString() string {
	formatted := h.Birthday.Format("02.01.2006")
	return formatted
}

func main() {

	hackers := []hacker{
		{
			FirstName: "Susanne",
			LastName:  "Breger",
			Birthday:  createBDay("1982-02-05 +0200"),
		},
		{
			FirstName: "Gernot",
			LastName:  "Hassknecht",
			Birthday:  createBDay("1975-01-23 +0200"),
		},
	}

	for _, h := range hackers {
		fmt.Printf("%s %s's Birthday: %s\n", h.FirstName, h.LastName, h.bDayToString())
	}
}
```

## 7

The program should print the current age of each person. Use the code in `bday/diff.go`

```go
package main

import (
	"fmt"
	"time"
	"gitlab.com/3stadt/garage-happy-birthday/bday"
)

type hacker struct {
	FirstName string
	LastName  string
	Birthday  time.Time
}

func createBDay(value string) time.Time {
	bDay, _ := time.Parse("2006-01-02 -0700", value)
	return bDay
}

func (h hacker) bDayToString() string {
	formatted := h.Birthday.Format("02.01.2006")
	return formatted
}

func main() {

	hackers := []hacker{
		{
			FirstName: "Susanne",
			LastName:  "Breger",
			Birthday:  createBDay("1982-12-05 +0200"),
		},
		{
			FirstName: "Gernot",
			LastName:  "Hassknecht",
			Birthday:  createBDay("1975-01-23 +0200"),
		},
	}

	currentYear := time.Now()

	for _, h := range hackers {
		age, _, _, _, _, _  := bday.Diff(h.Birthday, currentYear)
		fmt.Printf("%s %s's Birthday: %s\n", h.FirstName, h.LastName, h.bDayToString())
		fmt.Printf("Their age is: %d years\n--------\n", age)
	}
}
```

## 8

If the persons birthday is in the future, skip printing the birthday for that person

```go
package main

import (
	"fmt"
	"gitlab.com/3stadt/garage-happy-birthday/bday"
	"time"
)

type hacker struct {
	FirstName string
	LastName  string
	Birthday  time.Time
}

func timeFromString(value string) time.Time {
	timeVal, _ := time.Parse("2006-01-02 -0700", value)
	return timeVal
}

func (h hacker) bDayToString() string {
	return timeToString(h.Birthday)
}

func timeToString(t time.Time) string {
	return t.Format("02.01.2006")
}

func main() {

	hackers := []hacker{
		{
			FirstName: "Susanne",
			LastName:  "Breger",
			Birthday:  timeFromString("1982-12-05 +0200"),
		},
		{
			FirstName: "Gernot",
			LastName:  "Hassknecht",
			Birthday:  timeFromString("1975-01-23 +0200"),
		},
	}

	checkYear := timeFromString("1980-01-01 +0200")

	for _, h := range hackers {
		fmt.Printf("%s %s's Birthday: %s\n", h.FirstName, h.LastName, h.bDayToString())
		age, _, _, _, _, _ := bday.Diff(h.Birthday, checkYear)
		if h.Birthday.After(checkYear) {
			fmt.Printf("They will be born %d years after %s\n--------\n", age, timeToString(checkYear))
		} else {
			fmt.Printf("Their age at %s is: %d years\n--------\n", timeToString(checkYear), age)
		}
	}
}
```
