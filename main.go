package main

import (
	"fmt"
	"gitlab.com/3stadt/garage-happy-birthday/bday"
	"time"
)

type hacker struct {
	FirstName string
	LastName  string
	Birthday  time.Time
}

func timeFromString(value string) time.Time {
	timeVal, _ := time.Parse("2006-01-02 -0700", value)
	return timeVal
}

func (h hacker) bDayToString() string {
	return timeToString(h.Birthday)
}

func timeToString(t time.Time) string {
	return t.Format("02.01.2006")
}

func main() {

	hackers := []hacker{
		{
			FirstName: "Susanne",
			LastName:  "Breger",
			Birthday:  timeFromString("1982-12-05 +0200"),
		},
		{
			FirstName: "Gernot",
			LastName:  "Hassknecht",
			Birthday:  timeFromString("1975-01-23 +0200"),
		},
	}

	checkYear := timeFromString("1980-01-01 +0200")

	for _, h := range hackers {
		fmt.Printf("%s %s's Birthday: %s\n", h.FirstName, h.LastName, h.bDayToString())
		age, _, _, _, _, _ := bday.Diff(h.Birthday, checkYear)
		if h.Birthday.After(checkYear) {
			fmt.Printf("They will be born %d years after %s\n--------\n", age, timeToString(checkYear))
		} else {
			fmt.Printf("Their age at %s is: %d years\n--------\n", timeToString(checkYear), age)
		}
	}
}
